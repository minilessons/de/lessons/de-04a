
<p>Proučavanje Booleovih funkcija važno je zbog mnogih razloga. Booleove funkcije omogućavaju nam formalno opisivanje ponašanja kombinacijskog sklopovlja, njegovu analizu i modificiranje. Jednom kada imamo ponašanje kombinacijskog sklopa opisano Booleovom funkcijom, možemo se osloniti na postulate i aksiome Booleove algebre kako bismo nad algebarskim zapisom Booleove funkcije radili promjene koje neće promijeniti samu funkciju ali koje će rezultirati poželjnijim algebarskim zapisom funkcije na temelju kojeg ćemo moći ostvariti drugačiju, poželjniju sklopovsku realizaciju u odnosu na onu početnu.</p>

<p>Na što se pri tome odnosi termin "poželjnija" sklopovska realizacija? Da bismo odgovorili na to pitanje, moramo se najprije upoznati s nekoliko povezanih pojmova.</p>

<p><span style="font-weight: bold;">Broj razina logike</span>. Promotrimo sklop koji ostvaruje Booleovu funkciju \(y=f(A,B,\ldots)\) gdje su \(A\), \(B\), ... varijable odnosno ulazi sklopa. Pojam <em>broj razina logike</em> govori nam koliko sklopova postoji na putu od svakog od ulaza pa do izlaza \(y\). Ako na putevima različitih varijabli taj broj varira, kao broj razina logike sklopovskog ostvarenja uzima se onaj najveći.</p>

<p>Broj razina logike važan je pri procjeni brzine rada nekog sklopovskog ostvarenja kada se uzimaju u obzir stvarna kašnjenja digitalnih sklopova. Primjerice, sklopovsko ostvarenje izvedeno uz dvije razine logike, nakon što se promijeni neka od ulaznih varijabli, prije će uspjeti na izlazu izgenerirati novu korektnu vrijednost no što bi to bio slučaj kod sklopovskog ostvarenja s tri razine logike. U ovom prvom slučaju na putu od ulazne varijable pa do izlaza nalaze se samo dva sklopa, pa ako svaki unosi kašnjenje od primjerice 10 ns, izlaz sklopa će poprimiti novu vrijednost koja odgovara kombinaciji ulaznih varijabli nakon \(2 \cdot 10\)ns = 20 ns. U slučaju trorazinskog ostvarenja, promjena ulaza morat će promijeniti izlaze tri slijedno povezana sklopa (izlaz prvoga je ulaz drugoga, izlaz drugoga je ulaz trećega), čime će ukupno kašnjenje takvog sklopovskog ostvarenja biti 30 ns.</p>

<p>Pri određivanju broja razina logike, osim ako to nije eksplicitno drugačije zadano, radimo uz pretpostavku da na ulazima možemo dobiti vrijednosti varijabli i njihovih komplemenata istovremeno, pa u tom slučaju, iako u shemi sklopa možda postoje eksplicitno nacrtani invertori ulaznih varijabli, njih zanemarujemo (oni ne povećavaju broj razina logike).</p>

<p>Idemo ovo ilustrirati na primjeru. U nastavku su prikazane četiri sklopovska ostvarenja iste Booleove funkcije. Primijetite, razine brojimo počev od izlaza.</p>

<figure class="mlFigure">
<img src="@#file#(shema2a-ano-hr.svg)" alt="Ostvarenje funkcije uz dvije razine logike"><br>
<figcaption><span class="mlFigKey">Slika 1.a.</span> Ostvarenje funkcije uz dvije razine logike</figcaption>
</figure>

<figure class="mlFigure">
<img src="@#file#(shema2b-ano-hr.svg)" alt="Ostvarenje funkcije uz dvije razine logike"><br>
<figcaption><span class="mlFigKey">Slika 1.b.</span> Ostvarenje funkcije uz dvije razine logike i nešto manje sklopova</figcaption>
</figure>

<figure class="mlFigure">
<img src="@#file#(shema2c-ano-hr.svg)" alt="Ostvarenje funkcije uz dvije razine logike"><br>
<figcaption><span class="mlFigKey">Slika 1.c.</span> Ostvarenje funkcije uz tri razine logike i samo dvoulaznim logičkim sklopovima</figcaption>
</figure>

<figure class="mlFigure">
<img src="@#file#(shema2d-ano-hr.svg)" alt="Ostvarenje funkcije uz dvije razine logike"><br>
<figcaption><span class="mlFigKey">Slika 1.d.</span> Ostvarenje funkcije uz dvije razine logike i najmanje sklopova</figcaption>
</figure>

<p>Na svim slikama, lijevo se nalazi polje koje razvodi varijable i njihove komplemente. Invertore koji se tu nalaze ćemo zanemariti pri izračunu broja razina logike. Pogledajte sliku 1.a. Ako se, primjerice, promijeni vrijednost varijable A (a time i njezinog komplementa), ta se nova vrijednost istovremeno dovodi do svih logičkih sklopova I. Kako svi logički sklopovi I u istom trenutku dobivaju novu vrijednost varijable A (odnosno njezinog komplementa), nakon vremena kašnjenja logičkog sklopa I na izlazima svih petero logičkih sklopova pojavljuju se novoizračunate vrijednosti koje u istom trenutku dolaze na ulaze logičkog sklopa ILI. Nakon vremena kašnjenja logičkog sklopa ILI ispravna konačna vrijednost pojavit će se njegovom izlazu što je ujedno i izlaz sklopa. Da bi se ovo dogodilo, morali smo pričekati da prođu vremena kašnjenja dvaju logičkih sklopova, odnosno dvije razine logike.</p>

<p>Razine logike obično promatramo od izlaza sklopa. Logički sklop koji generira izlaz čini prvu razinu logike. Sklopovi od kojih on dobiva ulaze čine drugu razinu logike. Sklopovi od kojih oni dobivaju ulaze čine treću razinu logike, i tako dalje. Ovo je lijepo ilustrirano na slici 1.c koja sadrži tri razine logike. Pogledajte gornju granu - kada se promijeni A ili C, najprije treba sačekati da sklop ILI izračuna koliko je \(A + \bar C\), potom ta vrijednost mora proći kroz sklop I koji će je pomnožiti s \(\bar B\) i potom to sve mora proći kroz izlazni ILI-sklop; imamo tri razine logike.</p>

<p><span style="font-weight: bold;">Mjera "lošoće" sklopa</span>. Sljedeći pojam s kojim ćemo se upoznati jest mjera koja nam govori koliko je ostvarenje sklopa "loše" (interesantno kako u hrvatskom imamo riječ "dobrota" ali ne i njezin antonim). Kada razmatramo koliko je ostvarenje sklopa loše, možemo analizirati niz karakteristika tog ostvarenja. Neke karakteristike navedene su u nastavku.</p>

<ul>
<li><em>Koliko je ukupno utrošeno logičkih sklopova</em>. Pri tome nije bitno koje vrste i s koliko ulaza. Gledajući samo ovaj kriterij, ostvarenje koje treba 10 logičkih sklopova lošije je od ostvarenja koje treba 5 logičkih sklopova.</li>
<li><em>Koliko ukupno ulaza imaju svi logički sklopovi</em>. Broj ulaza kod osnovnih logičkih sklopova govori nam o njihovoj složenosti (a time potencijalno i o njihovoj cijeni, kašnjenjima i sličnome). Prema ovom kriteriju, ostvarenje koje ukupno ima 30 ulaza sumirano po logičkim sklopovima lošije je od ostvarenja koje ukupno ima 20 ulaza.</li>
<li><em>Broj razina logike.</em> Što sklop ima više razina logike, to je lošiji jer će biti sporiji odnosno trebat ćemo duže čekati od trenutka kada promijenimo vrijednosti ulaznih varijabli pa do trenutka kada se na izlazu sklopa pojavi nova ispravna vrijednost.</li>
</ul>

<p>Pogledajmo kako se nose ostvarenja sa slika 1.a do 1.d s obzirom na ove kriterije.</p>

<div class="mlTableCaption"><span class="mlTblKey">Tablica 1.</span> Usporedba sklopovskih ostvarenja sa slika 1.a do 1.d.</div>
<table class="mlTableCC mlTableVRl3">
<thead>
<tr><th>Sklop</th><th>Broj razina logike</th><th>Ukupni broj sklopova</th><th>Ukupni broj ulaza</th></tr>
</thead>
<tbody>
<tr><td>1.a.</td><td>2</td><td>6</td><td>20</td></tr>
<tr><td>1.b.</td><td>2</td><td>4</td><td>9</td></tr>
<tr><td>1.c.</td><td>3</td><td>4</td><td>8</td></tr>
<tr><td>1.d.</td><td>2</td><td>3</td><td>7</td></tr>
</tbody>
</table>

<p>Sklop prikazan na slici 1.d ovdje je definitivni pobjednik: ostvaren je u najmanje razina logike (pa su ukupna kašnjenja mala), troši najmanje logičkih sklopova (samo 3) i sklopovi sveukupno imaju najmanje ulaza (samo 7).</p>

<p>U praksi, međutim, situacija neće biti tako jednostavna. Primjerice, poznato je da općenito postoji kompromis između broja razina logike i broja logičkih sklopova: što je broj razina logike manji (težimo ka bržem radu sklopa), realizacija će (općenito) trebati više logičkih sklopova; s druge strane, ako broj razina logike povećavamo, padat će broj potrebnih logičkih sklopova (štedimo, ali sklop postaje sve sporiji).</p>

<div class="mlImportant"><p><span class="mlBold">Minimizacija Booleove funkcije</span></p>
<p>Postupak kojim se uz zadanu mjeru lošoće (koja je definirana nizom kriterija i načinom njihovog kombiniranja) pronalazi optimalno sklopovsko ostvarenje Booleove funkcije koje minimizira tu mjeru naziva se <em>minimizacija Booleove funkcije</em>.</p>
<p>Naziv "minimizacija" ne znači da se Booleova funkcija mijenja: ono što se mijenja je algebarski izraz kojim se ta funkcija prikazuje i na temelju kojega se izravno gradi sklopovsko ostvarenje funkcije.</p>
</div>

<p>Minimizacija Booleove funkcije općenito je vrlo složen postupak i često ne znamo kako doći do minimalnog oblika. Primjerice, zamislite da je zadatak pronaći ostvarenje zadane Booleove funkcije koje troši minimalnu količinu dvoulaznih sklopova NI. Za tako jednostavan zadatak danas još uvijek ne znamo napisati ikakav pametniji algoritam (a koji ne bi bio slijepo generiranje svih mogućih načina spajanja počev od jednog logičkog sklopa pa na više u nadi da će nam se posrećiti pa ćemo brzo nabasati na ostvarenje zadane Booleove funkcije, ili eventualno nekakva heuristika).</p>

<p>Za neke vrste minimizacije napravljeni su prikladni algoritmi i mi ćemo se upoznati s dva postupka minimizacije: K-tablicama te postupkom Quine-McCluskey. Pri tome je važno zapamtiti kako je definirana mjera lošoće koju ti postupci minimiziraju. Postupak minimizacije K-tablicama te postupak minimizacije Quine-McCluskey traže ostvarenja Booleove funkcije koja imaju minimalni broj razina logike a svakako ne više od dvije razine (dakle, koja su oblika sume varijabli, produkta varijabli, sume produkata varijabli ili pak produkta suma varijabli; "varijable" se ovdje odnose i na njihove komplemente), koja unutar svih ostvarenja s minimalnim brojem razina logike troše minimalni broj logičkih sklopova, a unutar svih takvih ostvarenja koja imaju minimalni broj razina logike i troše minimalni broj logičkih sklopova dalje imaju minimalni ukupni broj ulaza u sve logičke sklopove.</p>

<p>Dvorazinske izvedbe Booleove funkcije o kojima ovdje govorimo su suma produkata (koristi se još pokrata SOP - engl. <i>Sum-Of-Products</i>) odnosno produkt suma (koristi se još pokrata POS - engl. <i>Product-Of-Sums</i>). Izvedba sumom produkata prikazana je na slikama 1.a i 1.b; radi se o ostvarenju kod kojeg imamo jedan (izlazni) sklop ILI koji "sumira" produkte (izlaze sklopova I) na koje se dovode izravno varijable i njihovi komplementi. Izvedba produktom suma prikazana je na slici 1.d; radi se o ostvarenju kod kojeg imamo jedan (izlazni) sklop I koji "množi" sume (izlaze sklopova ILI) na koje se dovode izravno varijable i njihovi komplementi.</p>

<p>Prije upoznavanja s navedenim minimizacijskim postupcima pogledat ćemo kako se minimizacija može napraviti izravno algebarski te potom kako se Booleova funkcija prikazuje u K-tablici. Jednom kad to savladamo, pogledat ćemo kako se funkcija minimizira nakon što je upisana u K-tablicu.</p>

