function Cell(x, y, i, j, size, index, code, bit, context)
{
  this.x = x;
  this.y = y;
  this.i = i;
  this.j = j;
  this.size = size;
  this.index = index;
  this.code = code;
  this.bit = bit;
  this.context = context;

  this.stateDrawer = new CellStateDrawer(this);
  var padding = 4;
  this.color = null;
  this.padding = null;

  this.drawState = function(state) {
    var fillArea = true;
    this.stateDrawer.cellState[state].draw(this.context);
    if (fillArea) {
      this.stateDrawer.cellState[state].fill(this.context);
    }
  }

  this.clone = function() {
    return new Cell(this.x, this.y, this.i, this.j, this.size, this.index, this.code, this.bit, context);
  }
}
