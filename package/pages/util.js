function Point(x, y){
    this.x = x;
    this.y = y;
}

function decimalToGray(i, n) {
  var out = new Array(n);
  for (var j = 0; j < n - 1; ++j) {
    out[n-j-1] = ((i >> j) ^ (i >> j+1)) & 1;
  }
  out[0] = (i >> n-1) & 1;

  return out.join("");
}

function binaryToDecimal(s) {
  return parseInt(s, 2);
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function decimalToBinary(num, size) {
  return pad(num.toString(2), size);
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function fixIndex(index, limit) {
  if(index < 0) index = limit-1;
  if(index >= limit) index = 0;

  return index;
}

function powerOf2(n) {
  return n && (n & (n - 1)) === 0;
}

function allElementsEqual(array) {
  return array.every(function(v, i, a) {
     return i === 0 || v === a[i - 1];
  });
}

function binaryDistance(a, b) {
  var count = 0;
  for(var i = 0; i < a.length; ++i) {
    if (a[i] != b[i]) ++count;
  }

  return count;
}

function elipse(context, center_x, center_y, radius_x, radius_y){
    context.save();
    context.beginPath();
    context.translate(center_x - radius_x, center_y - radius_y);
    context.scale(radius_x, radius_y);
    context.arc(1, 1, 1, 0, 2 * Math.PI, false);
    context.restore();
    context.stroke();
}

function bezier(context, ax,ay, bx,by, cx,cy, dx,dy) {
  context.beginPath();
  context.moveTo(ax, ay);
  context.bezierCurveTo(bx, by, cx, cy, dx, dy);
  context.stroke();
}

function line(context, start_x, start_y, end_x, end_y) {
  context.beginPath();
  context.moveTo(start_x, start_y);
  context.lineTo(end_x, end_y);
  context.stroke();
}

function fillBezier(context, fillColor) {
  context.globalAlpha = 0.4;
  context.fillStyle = fillColor.getColor();
  context.fill();
}

function fillInsideOfBezier(context, fillColor, ax, ay, bx, by, cx, cy) {

  fillBezier(context, fillColor);

  context.fillStyle = fillColor.getColor();
  context.beginPath();
  context.moveTo(ax, ay);
  context.lineTo(bx, by);
  context.lineTo(cx, cy);
  context.fill();

  context.lineWidth = 0.4;
  context.strokeStyle = fillColor.getColorWithOpacity(0.4);
  context.closePath();
  context.stroke();
}

function fillRectangle(context, fillColor, ax, ay, width, height) {
  // context.globalAlpha = 0.4;
  context.fillStyle = fillColor.getColor();
  context.fillRect(ax, ay, width, height);
}
