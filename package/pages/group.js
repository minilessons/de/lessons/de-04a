function Group(implicants)
{
  this.implicants = implicants;
  this.color = new Color([getRandomInt(1, 256), getRandomInt(1,256), getRandomInt(1, 256)].join());
  this.opacity = '0.5';
  this.padding = getRandomInt(2, 5);
  this.cells = [];

  this.contains = function(implicant) {
    return this.implicants.indexOf(implicant) !== -1;
  }

  this.addCell = function(cell) {
    cell = cell.clone();
    cell.color = this.color;
    cell.padding = this.padding;
    cell.opacity = this.opacity;
    cell.stateDrawer = new CellStateDrawer(cell);
    this.cells.push(cell);
  }

  this.isValid = function(variablesLength, elements) {
    if(!powerOf2(this.implicants.length)) return false;
    // provjera - svi razliciti
    // provjera - svi unutar intervala

    // jedan zajednicki bit
    var validGroup = true;

    if(this.implicants.length > 1) {
      for(var i = 0; i < this.implicants.length; ++i) {
        var validCell = false;
        for(var j = 0; j < this.implicants.length;++j) {
          if (i == j) continue;

          if (binaryDistance(
                decimalToBinary(this.implicants[i], variablesLength),
                decimalToBinary(this.implicants[j], variablesLength)
              ) == 1) {

            validCell = true;
            break;
          }
        }

        if (!validCell) {
          validGroup = false;
        }
      }
    }

    if (!validGroup) return false;

    //ako je suma onda 1 ili x
    if (true) {
      return !this.implicants
        .map(function(implicant) { return elements[implicant]; })
        .some(function(item) { return item == '0' });
    }

    //ako je produkt onda 0 ili x
    if (false) {
      return !this.implicants
        .map(function(implicant) { return elements[implicant]; })
        .some(function(item) { return item == '1' });
    }

    return validGroup;
  }
}
