function Color (rgbColor) {
  this.rgb = rgbColor;
  this.opacity = 1;

  this.setRgbColor = function (rgbColor) {
    this.rgb = rgbColor;
  }

  this.setOpacity = function (opacity) {
    if (opacity < 0 || opacity > 1) {
      return;
    }
    this.opacity = opacity;
  }

  this.getColor = function() {
    return composeColor(this.rgb, this.opacity);
  }

  this.getColorWithOpacity = function(opacity) {
    return composeColor(this.rgb, opacity);
  }

  function removeOpacity() {
    this.opacity = 1;
  }

  function composeColor(rgbColor, opacity) {
    var rgb = rgbColor.replace(/[^\d,]/g, '');
    return "rgba(" + rgb + "," + opacity + ")";
  }
}
