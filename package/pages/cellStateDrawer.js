function CellStateDrawer(cell) {

  this.cell = cell;
  var x = cell.x;
  var y = cell.y;
  var size = cell.size;
  var padding = cell.padding;
  var context = cell.context;

  this.cellState = {};


  this.cellState.completeFill = {
    draw: function(context) {

    },
    fill: function(context) {
        fillRectangle(context, cell.color, x, y, size, size);
    }
  }


  this.cellState.top = {
    draw: function(context) {
      line(context, x, y + padding, x + size, y + padding);
    },
    fill: function(context) {
       fillRectangle(context, cell.color, x, y + padding, size, size - padding);
    }
  }

  this.cellState.bottom = {
      draw: function(context) {
        line(context, x, y + size - padding, x + size, y + size - padding);
      },
      fill: function(context) {
        fillRectangle(context, cell.color, x, y, size, size - padding);
      }
  }

  this.cellState.right = {
    draw: function(context) {
      line(context, x + size - padding, y, x + size - padding, y + size);
    },
    fill: function(context) {
      fillRectangle(context, cell.color, x, y, size - padding, size);
    }
  }

  this.cellState.left = {
    draw: function(context) {
      line(context, x + padding, y, x + padding, y + size);
    },
    fill: function(context) {
      fillRectangle(context, cell.color, x + padding, y, size - padding, size);
    }
  }


  this.cellState.leftRight = {
    draw: function(context) {
      line(context, x + padding, y, x + padding, y + size);
      line(context, x + size - padding, y, x + size - padding, y + size);
    },
    fill: function(context) {
      fillRectangle(context, cell.color, x + padding, y, size - 2*padding, size);
    }
  }

  this.cellState.topBottom = {
    draw: function(context) {
      line(context, x, y + padding, x+size, y + padding);
      line(context, x, y + size - padding, x + size, y + size - padding);
    },
    fill: function(context) {
      fillRectangle(context, cell.color, x, y + padding, size, size - 2*padding);
    }
  }

  this.cellState.topLeft = {
    draw: function(context) {
      bezier(context,
        x + padding, y + size,
        x + padding, y + size/4,
        x + size/4, y + padding,
        x + size, y + padding);
    },
    fill: function(context) {
      fillInsideOfBezier(context, cell.color, x + padding, y + size, x + size, y + padding, x + size, y + size);
    }
  }

  this.cellState.topRight = {
    draw: function(context) {
      bezier(context, x, y + padding,
        x + 3*size/4, y + padding,
        x + size - padding, y + size/4,
        x + size - padding, y + size);
    },
    fill: function(context) {
      fillInsideOfBezier(context, cell.color, x, y + padding, x, y + size, x + size - padding, y + size);
    }
  }

  this.cellState.bottomLeft = {
    draw: function(context) {
      bezier(context,
        x + padding, y,
        x + padding, y + 3*size/4,
        x + size/4, y + size - padding,
        x + size, y + size - padding);
    },
    fill: function(context) {
      fillInsideOfBezier(context, cell.color, x + padding, y, x + size, y, x + size, y + size-padding);
    }
  }

  this.cellState.bottomRight = {
    draw: function(context) {
      bezier(context,
        x, y + size - padding,
        x + 3*size/4, y + size - padding,
        x + size - padding, y + 3*size/4,
        x + size - padding, y);
    },
    fill: function(context) {
      fillInsideOfBezier(context, cell.color, x, y + size - padding, x, y, x + size - padding, y);
    }
  }


  this.cellState.topBottomLeft = {
    draw: function(context) {
      bezier(context,
        x + size, y + padding,
        x - size*0.3 + padding, y + padding,
        x - size*0.3 + padding, y + size - padding,
        x + size, y + size - padding);
    },
    fill: function(context) {
      fillBezier(context, cell.color);
    }
  }

  this.cellState.topBottomRight = {
    draw: function(context) {
      bezier(context,
        x, y + padding,
        x + size*1.3 - padding, y,
        x + size*1.3 - padding, y + size,
        x, y + size - padding );
    },
    fill: function(context) {
      fillBezier(context, cell.color);
    }
  }

  this.cellState.topLeftRight = {
    draw: function(context) {
      bezier(context, x + padding, y + size, x, y-size*0.3 + padding, x + size, y-size*0.3 + padding, x + size - padding, y + size);
    },
    fill: function(context) {
      fillBezier(context, cell.color);
    }
  }

  this.cellState.bottomLeftRight = {
    draw: function(context) {
      bezier(context, x + padding, y, x, y+size*1.3 - padding, x + size, y+size*1.3 - padding, x + size - padding, y);
    },
    fill: function(context) {
      fillBezier(context, cell.color);
    }
  }


  this.cellState.topBottomLeftRight = {
    draw: function(context) {
      elipse(context, x + size/2, y + size/2, size/2 - padding, size/2 - padding);
    },
    fill: function(context) {
      fillBezier(context, cell.color);
    }
  }

}
