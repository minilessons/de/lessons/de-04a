var KMap = function(config, context) {

  var size = 50;
  var padding = 13;
  var self = this;

  this.leftVariables = config.leftVariables;
  this.upVariables = config.upperVariables;
  this.elements = config.elements;
  this.variables = this.leftVariables + this.upVariables;
  this.context = context;

  this.showOrder = config.showOrder;
  this.showImplicants = config.showImplicants;
  this.isCellEditable = config.isCellEditable;
  this.isGroupEditable = config.isGroupEditable;
  this.multiCellSelect = config.multiCellSelect;

  this.rows = Math.pow(2, this.leftVariables.length);
  this.columns = Math.pow(2, this.upVariables.length);

  this.positions = new Array(this.rows).fill([]);
  this.positions.forEach(function(el, i) {
    self.positions[i] = new Array(self.columns).fill(0);
  });

  this.groups = config.groups || [];
  // this.groups = [ new Group([0, 2, 4, 6]), new Group([0, 4, 12, 8]) ];
  // this.groups = [[0,4,12,8,2,6,14,10]];
  // this.groups = [[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]];

  this.colors = [
    new Color('255,0,0'),
    new Color('128,255,0'),
    new Color('0,128,255'),
    new Color('255,0,255'),
    new Color('255,128,0'),
    new Color('255,255,0'),
    new Color('102,0,102'),
    new Color('95,93,40')
  ];

  this.selectedImplicants = [];
  this.elementValues = ['0', '1', 'X'];

  this.draw = function (canvas, context) {
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.beginPath();
      context.font = "14px Arial";
      context.textAlign = "center";
      context.textBaseline = "middle";

      context.stroke();

      var width = size * (this.columns+1);
      var height = size * (this.rows+1);

      var topLeft = new Point((canvas.width - width) / 2, (canvas.height - height) / 2);
      var x = topLeft.x;
      var y = topLeft.y;

      context.fillText(this.leftVariables, x, y + size + padding);
      context.fillText(this.upVariables, x + width - padding, y + padding);
      context.fillText("f(" + this.variables.split("").sort().join() + ")", x + size, y + padding);

      for (var i = 0; i < this.rows+1; i++) {
        x = topLeft.x;
        for (var j = 0; j < this.columns+1; j++) {
          var upBinary = decimalToGray(j-1, this.upVariables.length);
          var leftBinary = decimalToGray(i-1, this.leftVariables.length);

          if (i == 0 && j == 0) {
            // context.fillText(decimalToGray(j, 2), x + size/2, y + size/2);
          } else if (i == 0) {
            context.fillText(upBinary, x + size/2, y + size - padding);
          } else if (j == 0) {
            context.fillText(leftBinary, x + size - padding, y + size/2);
          } else {
            var code = leftBinary + upBinary;
            var index = this.calculateImplicant(this.variables, code);

            this.positions[i-1][j-1] = new Cell(
              x,
              y,
              i-1,
              j-1,
              size,
              index,
              code,
              this.elements[index],
              context
            );

            context.rect(x, y, size, size);
            context.save();
            context.globalAlpha = 1.0;
            fillRectangle(context, new Color('255,255,255'), this.positions[i-1][j-1].x, this.positions[i-1][j-1].y, this.positions[i-1][j-1].size, this.positions[i-1][j-1].size);
            context.restore();

            context.fillStyle = 'black';
            if(this.showOrder) {
              context.fillText(index, x + size/2, y + size/2);
            } else {
              context.fillText(this.elements[index], x + size/2, y + size/2);
            }

            if(this.selectedImplicants.indexOf(index) !== -1) {
              var cell = this.positions[i-1][j-1];
              context.save();
              context.globalAlpha = 0.5;
              fillRectangle(context, new Color('51,122,183'), this.positions[i-1][j-1].x, this.positions[i-1][j-1].y, this.positions[i-1][j-1].size, this.positions[i-1][j-1].size);
              context.globalAlpha = 1.0;
              context.restore();
            }
          }
          context.stroke();
          x += size;
        }
        context.stroke();
        y += size;
      }
      context.stroke();

  };

  this.calculateImplicant = function(variables, binary) {
    var value = binary.split("").map(function(el, i) {
      return { index: variables[i], value: el };
    }).sort(function(a, b) {
      return a.index.localeCompare(b.index);
    }).map(function(obj) {
      return obj.value;
    }).join("");

    return binaryToDecimal(value);
  }

  this.findCellByImplicant = function(implicant) {
    for(var i = 0 ; i < this.positions.length; ++i) {
      for(var j = 0; j < this.positions[i].length; ++j) {
        if(this.positions[i][j].index == implicant) return this.positions[i][j];
      }
    }

    return null;
  };

  this.getImplicant = function(i, j) {
    i = fixIndex(i, self.rows);
    j = fixIndex(j, self.columns);

    return self.positions[i][j].index;
  };

  this.isFullRow = function(group, i) {
    var fullRow = true;

    for(var y = 0; y < self.columns; ++y) {
      if(!group.contains(self.getImplicant(i, y))) fullRow = false;
    }

    return fullRow;
  };

  this.isFullCol = function(group, j) {
    var fullCol = true;

    for(var x = 0; x < self.rows; ++x) {
      if(!group.contains(self.getImplicant(x, j))) fullCol = false;
    }

    return fullCol;
  };

  this.encircleGroups = function() {
    while(this.colors.length < this.groups.length) {
      this.colors.push(new Color([getRandomInt(1, 256), getRandomInt(1,256), getRandomInt(1, 256)].join()));
    }

    this.groups.forEach(function(group, index) {

      group.color = self.colors[index];
      group.cells = [];
      group.implicants.forEach(function(implicant) {
        group.addCell(self.findCellByImplicant(implicant));
      });

      if(!group.isValid(self.variables.length, self.elements)) return;

      group.cells.forEach(function(cell) {
        var i = cell.i;
        var j = cell.j;
        var state = {
          top: !group.contains(self.getImplicant(i-1, j)),
          bottom: !group.contains(self.getImplicant(i+1, j)),
          left: !group.contains(self.getImplicant(i, j-1)),
          right: !group.contains(self.getImplicant(i, j+1))
        }

        if(self.isFullRow(group, i)) {
          if(j == 0) state.left = true;
          if(j == self.columns-1) state.right = true;
        }

        if(self.isFullCol(group, j)) {
          if(i == 0) state.top = true;
          if(i == self.rows-1) state.bottom = true;
        }

        self.encircleCell(cell, state);
      });
    });
  }

  this.encircleCell = function(cell, state) {
    var s = "";
    for (var edge in state) {
      if (state[edge]) {
        s = s.concat(edge.capitalize());
      }
    }

    if(s.length == 0) {
      s = 'completeFill';
    };

    cell.drawState(s.firstLower());

    // cell.cellState[s.firstLower()](context);
  }

  this.reset = function(canvas, context) {
    context.globalAlpha = 1.0;
    context.fillStyle = "rgb(255, 255, 255)";
    context.fillRect(0,0, canvas.width,canvas.height);
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.globalAlpha = 1.0;
    context.strokeStyle = new Color('0,0,0').getColorWithOpacity(1);
    context.fillStyle = new Color('0,0,0').getColorWithOpacity(1);
  };

  this.cellClick = function(cell) {
    if (this.isGroupEditable) {
      var i = this.selectedImplicants.indexOf(cell.index);
      if(i === -1) {
        if(this.multiCellSelect) {
          this.selectedImplicants.push(cell.index);
        } else {
          this.selectedImplicants = [cell.index];
        }
      } else {
        this.selectedImplicants.splice(i, 1);
      }
    }

    if (this.isCellEditable) {
      var index = cell.index;
      var element = this.elements[index];
      var i = (this.elementValues.indexOf(element) + 1) % this.elementValues.length;
      this.elements[index] = this.elementValues[i];
    }
  }

  this.addCircle = function() {
    this.groups.push(new Group(this.selectedImplicants));
    this.selectedImplicants = [];
  }

  String.prototype.capitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
  }

  String.prototype.firstLower = function() {
      return this.charAt(0).toLowerCase() + this.slice(1);
  }

}
