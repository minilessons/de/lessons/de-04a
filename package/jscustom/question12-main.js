// Vidljivi objekti:
// userData - mapa s podatcima koji se perzistiraju na poslužitelju
// questionCompleted - booleova zastavica

// metoda koja se poziva pri inicijalizaciji pitanja; u userData se može upisati sve što je potrebno...
function questionInitialize() {
  var config = [/*@#import#(elements${iid}.txt)*/][0];
  userData.leftVariables = config[0];
  userData.upperVariables = config[1];
  userData.elems = config[2];
  userData.expectedInput = [[2,3], [0,1,4,5]];
  userData.userInput = [];
}

// metoda koja se poziva kako bi napravila prikaz potrebne stranice; dobiva kao argument pogled koji je zadao korisnik
function questionRenderView(vid,infoParams) {
  if(vid=="start") {
    res = {};
    res.options = ['cont'];
    res.questionState = JSON.stringify({
      leftVariables: userData.leftVariables,
      upperVariables: userData.upperVariables,
      elems: userData.elems,
      userInput: userData.userInput
    });
    res.view = {action: "page:1"};
    return res;
  }
  if(vid=="done") {
    return {
      options: ['cont'],
      questionState: JSON.stringify({
        leftVariables: userData.leftVariables,
        upperVariables: userData.upperVariables,
        elems: userData.elems,
        userInput: userData.userInput
      }),
      view: {action: "page:4"}
    };
  }
  if(vid=="no-good") {
    var vars = {};
    vars.totalIncorrect = infoParams.inv;
    if(infoParams.er!=-1) {
      vars.row = infoParams.er;
      vars.error = true;
    } else {
      vars.row = infoParams.em;
      vars.error = false;
    }
    vars.lengthError = infoParams.lengthError;
    vars.firstError = infoParams.firstError;

    var actionPage = vars.lengthError ? "page:2" : "page:3";
    return {
      options: ['ret'],
      questionState: JSON.stringify({}),
      view: {action: actionPage},
      variables: vars
    };
  }
  return null;
}

// metoda koja se poziva kako bi se obradio poslan odgovor; prima kljuc (tj. opciju koja je pritisnuta) i objekt s poslanim podatcima
function questionProcessKey(vid, key, sentData) {
  if(vid=="start") {
    userData.userInput = sentData.userInput;
    var firstError = -1;
    var firstEmpty = false;
    var totalInvalid = 0;

    var haserr = false, lengthError = false, totalIncorrect = 0;
    if(userData.expectedInput.length != userData.userInput.length) {
      haserr = true;
      lengthError = true;
    } else {
      for (var i = 0; i < userData.userInput.length; i++) {
        var group = userData.userInput[i];
        var correct = false;

        for (var i = 0; i < userData.expectedInput.length; i++) {
          var correctGroup = userData.expectedInput[i];
          if(group.sort().join(',') === correctGroup.sort().join(',')) {
            correct = true;
            break;
          }
        }

        if (!correct) {
          haserr = true;
          firstError = group.join(', ');
          totalIncorrect++;
        }
      }
    }

    if(!haserr) {
      return {action: "view:done", completed: true};
    } else {
      return {action: "view:no-good", info: { er: haserr, lengthError: lengthError, totalIncorrect: totalIncorrect, firstError: firstError }};
    }
  } else if(vid=="done") {
    if(!questionCompleted) {
      return {action: "view:start"};
    } else {
      return {action: "next"};
    }
  } else if(vid=="no-good") {
    return {action: "view:start"};
  } else {
    return {action: "fail"};
  }
}
